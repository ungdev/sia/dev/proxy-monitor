import express, { ErrorRequestHandler, NextFunction, Request, Response } from 'express';
import cors from 'cors';
import helmet from 'helmet';
import proxy from 'express-http-proxy';
import axios from 'axios';

import { internalServerError } from './utils/responses';
import logger, { morgan } from './utils/logger';
import env from './utils/env';

const app = express();

const alert = () => {
  logger.warn('Send alert to slack');
  return axios.post(env.slack.url, {
    text: `Blackbox has not received any prometheus requests for ${env.slack.alertAfter}s`,
  });
};

let timeout = setTimeout(alert, env.slack.alertAfter * 1000);

// Logger
app.use(morgan());

// Security middlewares
app.use(cors(), helmet());

// Reset timeout at every requests
app.use((request: Request, response: Response, next: NextFunction) => {
  logger.silly('Reset timeout');
  clearTimeout(timeout);
  timeout = setTimeout(alert, env.slack.alertAfter * 1000);

  return next();
});

app.use(proxy(env.proxy.url));

// Error Handles
// The eslint disabling is important because the error argument can only be gotten in the 4 arguments function
// eslint-disable-next-line @typescript-eslint/no-unused-vars
app.use((error: ErrorRequestHandler, request: Request, response: Response, next: NextFunction) => {
  logger.error(error);
  return internalServerError(response);
});

export default app;
